from urllib.parse import urlparse
import logging
import time
import os
import json
import signal

import requests


CLIENTS_PATH = "/web/v1/controller?userStore&token="
LOGIN_PATH = "/api/user/login?ajax"


class WifiClient():
    """Wifi connected device."""

    def __init__(self, mac, name, total_traffic):
        self.mac = mac
        self.name = name
        self.total_traffic = total_traffic
        self.previous_total_traffic = None


class Daemon():

    def __init__(self, host, port, username, password, sleep_time, search_keys, ssl_verify=True, log_level="INFO"):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.sleep_time = sleep_time
        self.search_keys = search_keys
        self.ssl_verify = ssl_verify
        self.log_level = log_level 

        self.wifi_clients = {}

        self._session = None
        self._actual_url = None
        self._token = None
        self._must_run = True

        self._get_logger()

    def _get_logger(self):
        """Prepare logger."""
        logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s')
        self._logger = logging.getLogger('omada_cleaner')
        self._logger.setLevel(self.log_level)

    @property
    def full_host(self):
        return "http://" + self.host + ":" + self.port

    def _get_actual_url(self):
        if not self.ssl_verify:
            requests.packages.urllib3.disable_warnings()
        self._session = requests.Session()

        # Get sessionID
        res = self._session.get(self.full_host, verify=self.ssl_verify)

        # Get Actual URL
        actual_url = urlparse(res.history[-1].headers['location'])

        self._actual_url = actual_url.scheme + "://" + actual_url.netloc
        self._logger.info("Using URL: %s", self._actual_url)

    def _login(self):
        """Login and get token."""
        self._logger.info("Trying to login")
        data = {"method":"login",
                "params": {"name": self.username,
                           "password": self.password
                           }
               }
        res = self._session.post(self._actual_url + LOGIN_PATH,
                                 data=json.dumps(data),
                                 verify=self.ssl_verify)
        loggedin = res.json()['msg']
        if loggedin != 'Log in successfully.':
            self._logger.error("Log in Error ")
            raise
        self._logger.info("Log in successfully")

        # Get token
        self._token = res.json()['result']['token']

    def start(self):
        self._get_actual_url()
        self._login()
        self.run()

    def signal_handler(self, signum, frame):
        self._logger.info("Stopping daemon")
        self._must_run = False

    def run(self):
        signal.signal(signal.SIGINT, self.signal_handler)
        self._logger.info("Starting daemon")
        while self._must_run:

            for search_key in self.search_keys:
                self._logger.debug("Searching clients using search key: %s", search_key)
                self._fetch_client_list(search_key)
            self._disconnect_clients()

            sleep_timer = 0
            while sleep_timer < self.sleep_time and self._must_run:
                time.sleep(1)
                sleep_timer += 1

    def _fetch_client_list(self, search_key):
        """Get the wifi connected device list.

        List only devices matching the search key
        """
        current_page = 1
        currentPageSize = 10
        total_rows = currentPageSize + 1

        while current_page * currentPageSize <= total_rows:
            data = {"method":"getGridActiveClients",
                    "params": {"sortOrder": "asc",
                               "currentPage": current_page,
                               "currentPageSize": currentPageSize,
                               "filters": {"type":"all"},
                               "searchKey": search_key,
                               }
                    }
            res = self._session.post(self._actual_url + CLIENTS_PATH + self._token,
                                     data=json.dumps(data),
                                     verify=self.ssl_verify)
            total_rows = res.json()['result']['totalRows']
            current_page += 1
            results = res.json()
            for data in results['result']['data']:
                self._logger.debug("Guest device detected: (%s, %s)", data['mac'], data['name'])
                if data['mac'] in self.wifi_clients:
                    wifi_client = self.wifi_clients.get(data['mac'])
                    self._logger.debug("Guest device (%s, %s) traffic: %s to %s",
                                       data['mac'], data['name'],
                                       wifi_client.total_traffic,
                                       data['totalTraffic'])
                    wifi_client.previous_total_traffic = wifi_client.total_traffic
                    wifi_client.total_traffic = data['totalTraffic']
                else:
                    wifi_client = WifiClient(mac=data['mac'],
                                             name=data['name'],
                                             total_traffic=data['totalTraffic'],
                                             )
                    self.wifi_clients[wifi_client.mac] = wifi_client
                    self._logger.debug("Guest device (%s, %s) traffic: %s",
                                       data['mac'], data['name'], data['totalTraffic'])

    def _disconnect_clients(self):
        """Disconnect devices which have no traffic since last run."""
        client_to_delete = []
        for wifi_client_mac, wifi_client in self.wifi_clients.items():
            data = {"method":"clientReconnect",
                    "params":
                    {"clientMac": wifi_client.mac}}

            if wifi_client.total_traffic == wifi_client.previous_total_traffic:
                self._logger.info("Disconnectin %s, (%s)", wifi_client.name, wifi_client.mac)
                res = self._session.post(self._actual_url + CLIENTS_PATH + self._token,
                                         data=json.dumps(data),
                                         verify=self.ssl_verify)
                if res.json()['errorCode'] != 0:
                    self._logger.error(ret.json()['msg'])
                    raise
                client_to_delete.append(wifi_client.mac)
 
        # Delete deletec clients
        for mac in client_to_delete:
            del(self.wifi_clients[mac])


def main():
    """Main function."""
    host = os.environ.get("OMADA_HOST", "127.0.0.1")
    port = str(os.environ.get("OMADA_PORT", 8088))
    username = os.environ["OMADA_USERNAME"]
    password = os.environ["OMADA_PASSWORD"]
    search_keys = os.environ["OMADA_SEARCH_KEYS"].split(",")
    ssl_verify = os.environ.get("OMADA_SSL_VERIFY", True)
    log_level = os.environ.get("LOG_LEVEL", "INFO")
    if ssl_verify.lower() in ["false", 0, "no"]:
        ssl_verify = False
    sleep_time = float(os.environ.get("SLEEP_TIME", 60))
    daemon = Daemon(host, port,
                    username, password,
                    sleep_time,
                    search_keys,
                    ssl_verify,
                    log_level,
                    )
    daemon.start()


if __name__ == "__main__":
    main()
