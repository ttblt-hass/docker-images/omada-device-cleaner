import sys

from setuptools import setup
from omada_cleaner.__version__ import VERSION

if sys.version_info < (3,4):
    sys.exit('Sorry, Python < 3.4 is not supported')

install_requires = list(val.strip() for val in open('requirements.txt'))
#tests_require = list(val.strip() for val in open('test_requirements.txt'))

setup(name='omada-cleaner',
      version=VERSION,
      description='Remove disconnected devices from omada UI',
      author='Thibault Cohen',
      author_email='titilambert@ttb.lt',
      url='https://gitlab.com/ttblt-hass/docker-images/omada-device-cleaner',
      include_package_data=True,
      packages=['omada_cleaner'],
      entry_points={
          'console_scripts': [
              'omada-cleaner = omada_cleaner.__main__:main',
          ]
      },
      license='Apache 2.0',
      install_requires=install_requires,
      #tests_require=tests_require,
      classifiers=[
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
      ]
)

