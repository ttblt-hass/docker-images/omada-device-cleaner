FROM python:3.8-buster

RUN mkdir -p /src/

WORKDIR /src

ADD requirements.txt /src/

RUN pip install -r requirements.txt

ADD setup.py setup.cfg README.rst /src/
ADD omada_cleaner/ /src/omada_cleaner/

RUN python setup.py install

CMD omada-cleaner
